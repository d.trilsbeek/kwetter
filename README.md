# Microservices Info And Ports

| Microservice | Port |
| ------------ | ---- |
| RabbitMQ              | 15672 / 5672 |
| API-Gateway           | 5000 |
| Identity              | 5001 |
| Tweets                | 5010 |
| Tweets.Cache          | 5011 |
| Users                 | 5020 |
| Follows               | 5030 |


# Set Secrets
```
setx ConnectionStrings__AzureServiceBusConnection "" /M
setx ConnectionStrings__MySql "" /M
```

# Azure Service Bus

## Create Topics CLI
```
az servicebus topic create --resource-group kwetter  --namespace-name kwetter-test --name user.request-deletion

```