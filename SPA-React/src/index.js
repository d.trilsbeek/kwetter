import React from "react"
import ReactDOM from "react-dom"
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom"

import "assets/plugins/nucleo/css/nucleo.css"
import "@fortawesome/fontawesome-free/css/all.min.css"
import "assets/scss/argon-dashboard-react.scss"
import "typeface-poppins"

import AdminLayout from "layouts/Admin.js"
import AuthLayout from "layouts/Auth.js"
import UserLayout from "layouts/User.js"
import { AuthProvider } from "oidc-react"
import axios from "axios"

axios.defaults.baseURL = process.env.REACT_APP_AXIOS_BASE_URL
const identityBaseURL = process.env.REACT_APP_IDENTITY_BASE_URL
const identityClientId = process.env.REACT_APP_IDENTITY_CLIENT_ID
const baseURL = process.env.REACT_APP_BASE_URL

const oidcConfig = {
	onSignIn: (user) => {
		console.log(user)
	},
	authority: identityBaseURL,
	clientId: identityClientId,
	redirectUri: baseURL+"/user/home",
	responseType: "code",
	scope: "openid profile kwetter",
	postLogoutRedirectUri: baseURL,
	prompt: true,
}

ReactDOM.render(
	<AuthProvider {...oidcConfig}>
		<BrowserRouter>
			<Switch>
				{/*<Route path="/callback" render={(props) => <Callback {...props} />} />*/}
				<Route path="/admin" render={(props) => <AdminLayout {...props} />} />
				<Route path="/auth" render={(props) => <AuthLayout {...props} />} />
				<Route path="/user" render={(props) => <UserLayout {...props} />} />
				<Redirect from="/" to="/admin/index" />
			</Switch>
		</BrowserRouter>,
	 </AuthProvider>,

document.getElementById("root")
)
