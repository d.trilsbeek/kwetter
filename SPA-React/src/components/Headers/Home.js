/*!

=========================================================
* Argon Dashboard React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from 'react'

// reactstrap components
import { Container } from 'reactstrap'
import Pill from '../trends/Pill'


const Home = () => {
  return (
	<>
	  <div className='header bg-gradient-info pb-8 pt-3 pt-md-5'>
		<Container>
		  <div className='header-body'>
			{/* Card stats */}
			<h2 className='text-white mb-0'>Trends</h2>

			<Pill>
			  #DotNet5
			</Pill>

			<Pill>
			  #EFCore
			</Pill>

			<Pill>
			  #Kubernetes
			</Pill>

			<Pill>
			  #RabbitMQ
			</Pill>

		  </div>
		</Container>
	  </div>
	</>
  )
}

export default Home
