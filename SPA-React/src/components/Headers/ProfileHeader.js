/*!

=========================================================
* Argon Dashboard React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react"

// reactstrap components
import { Col, Container, Row } from "reactstrap"

const ProfileHeader = () => {
  return (
    <>
      <div
        className="header pb-8 pt-3 pt-md-5 d-flex align-items-center"
      >
        {/* Mask */}
        <span className="mask bg-gradient-primary opacity-8" />
        {/* Header container */}
        <Container className="d-flex align-items-center">
          <Row>
            <Col lg="7" md="10">
              <h1 className="display-2 text-white">Profile</h1>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default ProfileHeader;
