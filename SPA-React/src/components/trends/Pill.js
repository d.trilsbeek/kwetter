/*!

=========================================================
* Argon Dashboard React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from 'react'

// reactstrap components
import { Card, CardBody, CardTitle } from 'reactstrap'
import styled from 'styled-components'

const StyledCard = styled(Card)`
  display: inline-block;
  border-radius: 25px;
`
const StyledCardBody = styled(CardBody)`
  padding: 5px 10px;
`

const Pill = (props) => {
  return (
	<>
	  <StyledCard className='mb-4 mb-xl-0 mr-2'>
		<StyledCardBody>
		  <CardTitle
			tag='h5'
			className='text-uppercase text-muted mb-0'
		  >
			{props.children}
		  </CardTitle>
		</StyledCardBody>
	  </StyledCard>
	</>
  )
}

export default Pill
