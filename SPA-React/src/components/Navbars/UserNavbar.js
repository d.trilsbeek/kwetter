/*!

=========================================================
* Argon Dashboard React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import {Link} from "react-router-dom";
// reactstrap components
import {
	UncontrolledCollapse,
	NavbarBrand,
	Navbar,
	NavItem,
	NavLink,
	Nav,
	Container,
	Row,
	Col, Button
} from "reactstrap"
import KwetterLogo from "../brand/KwetterLogo";
import { useAuth } from "oidc-react"

const UserNavbar = () => {

	const auth = useAuth();

	const ShowNavItem = auth.userData ? <NavItem>
		<NavLink
			className="nav-link-icon"
			to="/user/profile"
			tag={Link}
		>
			<i className="ni ni-single-02"/>
			<span className="nav-link-inner--text">Profile</span>
		</NavLink>
	</NavItem>
		:
	<NavItem>
		<Button onClick={auth.signIn}>
			<i className="ni ni-single-02"/>
			<span className="nav-link-inner--text">Log In</span>
		</Button>
	</NavItem>

	return (
		<>
			<Navbar className="navbar-top navbar-horizontal navbar-light" expand="md">
				<Container className="px-4">
					<NavbarBrand to="/user/home" tag={Link}>
						<KwetterLogo/>
					</NavbarBrand>
					<button className="navbar-toggler" id="navbar-collapse-main">
						<span className="navbar-toggler-icon"/>
					</button>
					<UncontrolledCollapse navbar toggler="#navbar-collapse-main">
						<div className="navbar-collapse-header d-md-none">
							<Row>
								<Col className="collapse-brand" xs="6">
									<Link to="/">
									  <KwetterLogo/>
									</Link>
								</Col>
								<Col className="collapse-close" xs="6">
									<button className="navbar-toggler" id="navbar-collapse-main">
										<span/>
										<span/>
									</button>
								</Col>
							</Row>
						</div>
						<Nav className="ml-auto" navbar>
							{ShowNavItem}
						</Nav>
					</UncontrolledCollapse>
				</Container>
			</Navbar>
		</>
	);
};

export default UserNavbar;
