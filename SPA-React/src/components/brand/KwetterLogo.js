import React from 'react'
import styled from 'styled-components'
import LogoSvg from "./LogoSvg";

const StyledLogo = styled(LogoSvg)`
	display: inline-block;
	height: 70px;

	@media (max-width: 750px) {
		height: 50px;
	}
`

export default function KwetterLogo() {
    return <StyledLogo />
}
