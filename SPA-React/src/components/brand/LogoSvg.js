import React from 'react'

import styled from 'styled-components'
import { ReactComponent as Logo } from './twitter-svgrepo-com.svg'

const StyledRoot = styled.div`
	& svg {
		height: 100%;
	}
`

export default function LogoSvg(props) {
	return (
		<StyledRoot {...props}>
			<Logo />
		</StyledRoot>
	)
}
