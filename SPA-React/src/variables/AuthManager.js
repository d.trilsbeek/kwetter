import Oidc from "oidc-client"

const config = {
	authority: "https://localhost:5001",
	client_id: "js",
	redirect_uri: "http://localhost:3000/callback",
	response_type: "code",
	scope: "openid profile kwetter",
	post_logout_redirect_uri: "http://localhost:3000"
}
const mgr = new Oidc.UserManager(config)

class AuthManager {

	constructor() {
		mgr.getUser().then((user) => {
			if (user) {

				this.user = user;
				console.log("User logged in", user.profile)
			} else {
				console.log("User not logged in")
			}
		})
	}

	login() {
		console.log("test")
		mgr.signinRedirect().then(() => {
			console.log("test2")
			mgr.getUser().then((user) => {
				this.user = user;
			})
			console.log("Sign in redirected")
		})
	}

	api() {
		mgr.getUser().then((user) => {
			const url = "https://localhost:5010/api/tweets"

			const xhr = new XMLHttpRequest()
			xhr.open("GET", url)
			xhr.onload = () => {
				console.log(xhr.status, JSON.parse(xhr.responseText))
			}
			xhr.setRequestHeader("Authorization", "Bearer " + user.access_token)
			xhr.send()
		})
	}

	logout() {
		mgr.signoutRedirect().then(() => {
			this.user = null
			console.log("Sign out redirected")
		})
	}

	isLoggedIn() {
		return this.user != null
	}

	log() {
		Array.prototype.forEach.call(arguments, function(msg) {
			if (msg instanceof Error) {
				msg = "Error: " + msg.message
			} else if (typeof msg !== "string") {
				msg = JSON.stringify(msg, null, 2)
			}
			// document.getElementById("results").innerHTML += msg + "\r\n"
			console.log(msg + "\r\n");
		})
	}
}

export default (new AuthManager())

