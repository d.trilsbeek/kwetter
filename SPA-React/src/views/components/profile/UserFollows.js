import React from "react"
import axios from "axios"

// reactstrap components

class UserFollows extends React.Component {
	constructor(props, context) {
		super(props, context)

		this.state = {
			follows: {
				followers: "--",
				following: "--"
			}
		}
	}

	getFollows() {
		const username = this.props.username

		if (username) {
			axios
				.get(`/api/follows/users/${username}`)
				.then(({ data }) => {
					this.setState({
						follows: data
					})
				})
		}
	}

	componentDidUpdate(prevProps, prevState, snapshot) {
		if (prevProps.username !== this.props.username) {
			this.getFollows()
		}
	}

	render() {
		const { followers, following } = this.state.follows

		return (
			<div className="card-profile-stats d-flex justify-content-center mt-md-3">
				<div>
					<span className="heading">{followers}</span>
					<span className="description">Followers</span>
				</div>
				<div>
					<span className="heading">{following}</span>
					<span className="description">Following</span>
				</div>
				<div>
					<span className="heading">{this.props.tweetCount}</span>
					<span className="description">Tweets</span>
				</div>
			</div>
		)
	}
}

export default UserFollows
