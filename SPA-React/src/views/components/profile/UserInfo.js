import React from "react"
import { Button, Card, CardBody, CardHeader, Col, Row } from "reactstrap"
import axios from "axios"
import { withAuth } from "oidc-react"
import UserFollows from "./UserFollows"

// reactstrap components

class UserInfo extends React.Component {
	constructor(props, context) {
		super(props, context)

		this.state = {
			following: false,
		}

		this.follow = this.follow.bind(this)
	}

	follow(event) {
		event.preventDefault()

		const username = this.props.user.username

		console.log(username)
		const headers = {
			"Content-Type": "application/json",
			"Authorization": `Bearer ${this.props.userData.access_token}`
		}

		axios
		.post(`/api/follows`,
			{
				followingUsername: username
			},
			{
				headers: headers
			}
			)
		.then(({ data }) => {
			this.setState({
				following: true
			})
		})
	}

	render() {
		const { user } = this.props

		const following = this.state.following ? "Following": "Follow"

		return (
			<Card className="card-profile shadow">
				<Row className="justify-content-center">
					<Col className="order-lg-2" lg="3">
						<div className="card-profile-image">
							<button onClick={e => e.preventDefault()}>
								<img alt="..." className="rounded-circle" src={user.picture} />
							</button>
						</div>
					</Col>
				</Row>
				<CardHeader className="text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
					<div className="d-flex justify-content-between">
						<Button
							className="mr-4"
							color="info"
							href="#pablo"
							onClick={this.follow}
							size="sm"
						>
							{following}
						</Button>
						<Button
							className="float-right"
							color="default"
							href="#pablo"
							onClick={e => e.preventDefault()}
							size="sm"
						>
							Message
						</Button>
					</div>
				</CardHeader>
				<CardBody className="pt-0 pt-md-4">
					<Row>
						<div className="col">
							<UserFollows username={user.username} tweetCount={user.tweetCount}/>
						</div>
					</Row>
					<div className="text-center">
						<h3>
							{user.firstName} {user.lastName}
							<span className="font-weight-light">, @{user.username}</span>
						</h3>
						<div className="h5 mt-4">
							<i className="ni business_briefcase-24 mr-2" />
							{user.bio}
						</div>
						<div>
							<i className="ni location_pin mr-2 pt-2" />
							<small>Location:</small> {user.location}
						</div>
						<div>
							<i className="ni education_hat mr-2" />
							<small>University:</small> {user.university}
						</div>
						<hr className="my-4" />
						<a href={user.website}>{user.website}</a>
					</div>
				</CardBody>
			</Card>
		)
	}
}

export default withAuth(UserInfo)
