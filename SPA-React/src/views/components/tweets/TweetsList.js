import React from "react"
import Tweet from "./Tweet"

const TweetsList = props => {
	const { tweets } = props

	return (
		<>
			{tweets.map((tweet, key) => (
				<Tweet tweet={tweet} key={key} />
			))}
		</>
	)
}

export default TweetsList
