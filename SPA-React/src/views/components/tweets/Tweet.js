import React from "react"
import { Card, CardBody, CardTitle, Col, Row } from "reactstrap"
import { Link } from "react-router-dom"

// reactstrap components

const Tweet = props => {
	const { tweet } = props

	return (
		<Card className="card-stats mb-4 mb-xl-0">
			<CardBody>
				<Row>
					<Col className="col-md-2">
						<img
							src={tweet.pictureThumb}
							className="img-thumbnail img-center rounded-circle p-0 overflow-hidden"
							style={{
								height: 80,
								width: 80
							}}
							alt="PersonalProfile"
						/>
					</Col>

					<div className="col">
						<CardTitle tag="h5" className="text-muted mb-0 d-inline">
							{tweet.name}
						</CardTitle>

						<Link
							to={"/user/" + tweet.username}
							className="text-nowrap text-muted text-sm ml-1"
						>
							@{tweet.username}
						</Link>

						<div>
							<span className="font-weight-bold mb-0">
								{props.tweet.message}
							</span>
						</div>
					</div>
				</Row>
			</CardBody>
		</Card>
	)
}

export default Tweet
