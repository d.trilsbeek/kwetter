/*!

=========================================================
* Argon Dashboard React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react"

// reactstrap components
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  Row
} from "reactstrap"
// core components
import ProfileHeader from "components/Headers/ProfileHeader.js"
import axios from "axios"
import { withAuth } from "oidc-react"
import PersonalUserInfo from "./PersonalUserInfo"

class PersonalProfile extends React.Component {

  constructor(props, context) {
    super(props, context)
    this.state = {
      editable: {

      },
      user: {
        username: "",
        firstName: "",
        lastName: "",
        email:  "",
        bio: "..write something here",
        id: 0,
        university: "",
        inserted: "0001-01-01T00:00:00",
        lastUpdated: "0001-01-01T00:00:00",
        location: "",
        name: "",
        picture: "",
        website: "",
      },
    }
    this.handleInputChange = this.handleInputChange.bind(this);
    this.requestDeletion = this.requestDeletion.bind(this);
  }

  componentDidMount() {
    const userData = this.props.userData

    if (userData) {
      axios.get("/api/users/dtrilsbeek", {
        headers: {
          "Authorization": `Bearer ${userData.access_token}`
        }
      })
      .then( ({data}) => {
        this.setState({
          user: data,
        });
      })
    }
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState(prev => ({
      user: {
        ...prev.user,
        [name]: value
      }
    }));
  }

  requestDeletion() {
    if (window.confirm('Are you sure? This action cannot be undone')) {
      const headers = {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${this.props.userData.access_token}`
      }

      axios.post("/api/users/request-deletion", {} , {
        headers: headers
      })
      .then((data) => {
        this.props.signOutRedirect().then(r => {
          console.log(r)
        })
      })
    }
  }

  render() {
    const {user} = this.state;
    return (
      <>
        <ProfileHeader />
        {/* Page content */}
        <Container className="mt--7">
          <Row>
            <Col className="order-xl-2 mb-5 mb-xl-0" xl="4">
              <PersonalUserInfo user={this.state.user} />
            </Col>
            <Col className="order-xl-1" xl="8">
              <Card className="bg-secondary shadow">
                <CardHeader className="bg-white border-0">
                  <Row className="align-items-center">
                    <Col xs="8">
                      <h3 className="mb-0">My account</h3>
                    </Col>
                    <Col className="text-right" xs="4">
                      <Button
                        color="danger"
                        href="#"
                        onClick={this.requestDeletion}
                        size="sm"
                      >
                        Request for deletion
                      </Button>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  <Form>
                    <h6 className="heading-small text-muted mb-4">
                      User information
                    </h6>
                    <div className="pl-lg-4">
                      <Row>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-username"
                            >
                              Username
                            </label>
                            <Input
                              className="form-control-alternative"
                              name="username"
                              value={user.username ?? ""}
                              onChange={this.handleInputChange}
                              id="input-username"
                              placeholder="Username"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-email"
                            >
                              Email address
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-email"
                              placeholder="jesse@example.com"
                              name="email"
                              value={user.email ?? ""}
                              onChange={this.handleInputChange}
                              type="email"
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-first-name"
                            >
                              First name
                            </label>
                            <Input
                              className="form-control-alternative"
                              id="input-first-name"
                              placeholder="First name"
                              name="firstName"
                              value={user.firstName ?? ""}
                              onChange={this.handleInputChange}
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                        <Col lg="6">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-last-name"
                            >
                              Last name
                            </label>
                            <Input
                              className="form-control-alternative"
                              name="lastName"
                              value={user.lastName ?? ""}
                              onChange={this.handleInputChange}
                              id="input-last-name"
                              placeholder="Last name"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                    </div>
                    <hr className="my-4" />
                    {/* Address */}
                    <h6 className="heading-small text-muted mb-4">
                      Contact information
                    </h6>
                    <div className="pl-lg-4">
                      <Row>
                        <Col md="12">
                          <FormGroup>
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                              University
                            </label>
                            <Input
                              className="form-control-alternative"
                              name="university"
                              value={user.university ?? ""}
                              onChange={this.handleInputChange}
                              id="input-address"
                              placeholder="University name"
                              type="text"
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                    </div>
                    <hr className="my-4" />
                    {/* Description */}
                    <h6 className="heading-small text-muted mb-4">About me</h6>
                    <div className="pl-lg-4">
                      <FormGroup>
                        <label>About Me</label>
                        <Input
                          className="form-control-alternative"
                          placeholder="A few words about you ..."
                          rows="4"
                          name="bio"
                          value={user.bio ?? ""}
                          onChange={this.handleInputChange}
                          type="textarea"
                        />
                      </FormGroup>
                    </div>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    )
  }
}

export default withAuth(PersonalProfile);
