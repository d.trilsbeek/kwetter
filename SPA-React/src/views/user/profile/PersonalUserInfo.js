import React from "react"
import { Card, CardBody, CardHeader, Col, Row } from "reactstrap"
import { withAuth } from "oidc-react"

// reactstrap components

class PersonalUserInfo extends React.Component {

	render() {
		const { user } = this.props

		return (
			<Card className="card-profile shadow">
				<Row className="justify-content-center">
					<Col className="order-lg-2" lg="3">
						<div className="card-profile-image">
							<button onClick={e => e.preventDefault()}>
								<img alt="..." className="rounded-circle" src={user.picture} />
							</button>
						</div>
					</Col>
				</Row>
				<CardHeader className="text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
					<div className="d-flex justify-content-between">

					</div>
				</CardHeader>
				<CardBody className="pt-0 pt-md-4">
					<Row>
						<div className="col">
							<div className="card-profile-stats d-flex justify-content-center mt-md-3">
								<div>
									<span className="heading">22</span>
									<span className="description">Followers</span>
								</div>
								<div>
									<span className="heading">10</span>
									<span className="description">Following</span>
								</div>
								<div>
									<span className="heading">89</span>
									<span className="description">Tweets</span>
								</div>
							</div>
						</div>
					</Row>
					<div className="text-center">
						<h3>
							{user.firstName} {user.lastName}
							<span className="font-weight-light">, @{user.username}</span>
						</h3>
						<div className="h5 mt-4">
							<i className="ni business_briefcase-24 mr-2" />
							{user.bio}
						</div>
						<div>
							<i className="ni location_pin mr-2 pt-2" />
							<small>Location:</small> {user.location}
						</div>
						<div>
							<i className="ni education_hat mr-2" />
							<small>University:</small> {user.university}
						</div>
						<hr className="my-4" />
						<a href={user.website}>{user.website}</a>
					</div>
				</CardBody>
			</Card>
		)
	}
}

export default withAuth(PersonalUserInfo)
