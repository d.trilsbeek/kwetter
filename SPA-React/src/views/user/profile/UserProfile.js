/*!

=========================================================
* Argon Dashboard React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react"

// reactstrap components
import { Col, Container, Row } from "reactstrap"
// core components
import ProfileHeader from "components/Headers/ProfileHeader.js"
import axios from "axios"
import { withAuth } from "oidc-react"
import UserTweets from "./UserTweets"
import UserInfo from "../../components/profile/UserInfo"

class UserProfile extends React.Component {

  constructor(props, context) {
    super(props, context)
    this.state = {
      editable: {

      },
      user: {
        username: "",
        firstName: "",
        lastName: "",
        email:  "",
        bio: "..write something here",
        id: 0,
        university: "",
        inserted: "0001-01-01T00:00:00",
        lastUpdated: "0001-01-01T00:00:00",
        location: "",
        name: "",
        picture: "",
        website: "",
      },
    }
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  componentDidMount() {
    const username = this.props.match?.params.user

    axios
    	.get(`/api/users/${username}`)
    	.then(({ data }) => {
    		this.setState({
    			user: data
    		})
    	})
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState(prev => ({
      user: {
        ...prev.user,
        [name]: value
      }
    }));
  }

  render() {
    const username = this.props.match?.params.user

    return (
      <>
        <ProfileHeader />
        {/* Page content */}
        <Container className="mt--7">

          <Row>
            <Col className="" xl="8">
              <UserTweets username={username}/>
            </Col>
            <Col className="" xl={4} >
              <UserInfo user={this.state.user} />
            </Col>
          </Row>

        </Container>
      </>
    )
  }
}

export default withAuth(UserProfile);
