/*!

=========================================================
* Argon Dashboard React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react"
import axios from "axios"
import TweetsList from "../../components/tweets/TweetsList"
import { Card, CardBody, CardHeader, Row } from "reactstrap"

class HomeTweets extends React.Component {
	constructor(props, context) {
		super(props, context)

		this.state = {
			tweets: []
		}
	}

	componentDidMount() {
		axios.get(`/api/tweets`).then(({ data }) => {
			this.setState({
				tweets: data
			})
		})
	}

	render() {
		const { tweets } = this.state

		return (
			<Card className="shadow mt-4">
				<CardHeader className="bg-transparent">
					<Row className="align-items-center">
						<div className="col">
							<h6 className="text-uppercase text-muted ls-1 mb-1">Overview</h6>
							<h2 className="mb-0">Timeline</h2>
						</div>
					</Row>
				</CardHeader>
				<CardBody className="p-0">
					<TweetsList
						tweets={tweets}
					/>
				</CardBody>
			</Card>
		)
	}
}

export default HomeTweets
