/*!

=========================================================
* Argon Dashboard React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react"
import { Card, CardBody, CardHeader, Row } from "reactstrap"
import { withAuth } from "oidc-react"
import Tweet from "../../components/tweets/Tweet"

class UserTweets extends React.Component {
	constructor(props, context) {
		super(props, context)

		this.state = {
			tweets: []
		}
	}

	componentDidMount() {
		// const userId = this.props.userData?.profile.sub
		//
		// axios
		// 	.get(`http://localhost:5010/api/users/${userId}/tweets`)
		// 	.then(({ data }) => {
		// 		console.log(data)
		// 		this.setState({
		// 			tweets: data
		// 		})
		// 	})
	}

	render() {
		const { tweets } = this.state

		return (
			<Card className="shadow">
				<CardHeader className="bg-transparent">
					<Row className="align-items-center">
						<div className="col">
							<h6 className="text-uppercase text-muted ls-1 mb-1">
								Statistics
							</h6>
							<h2 className="mb-0">Your Tweets</h2>
						</div>
					</Row>
				</CardHeader>
				<CardBody className="p-0">
					{tweets.map((tweet, key) => (
						<Tweet tweet={tweet} key={key} />
					))}
				</CardBody>
			</Card>
		)
	}
}

export default withAuth(UserTweets)
