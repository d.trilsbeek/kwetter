/*!

=========================================================
* Argon Dashboard React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react"
import { Card, CardBody, CardHeader, Col, Container, FormGroup, Input, Row } from "reactstrap"
import Header from "components/Headers/Home.js"
import { withAuth } from "oidc-react"
import axios from "axios"
import UserTweets from "./UserTweets"
import HomeTweets from "./HomeTweets"

class Home extends React.Component {


  constructor(props, context) {
    super(props, context)

    this.state = {
      message: ""
    }

    this.setMessage = this.setMessage.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    const {message} = this.state

    const headers = {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${this.props.userData.access_token}`
    }

    axios.post("/api/tweets", {
      message: message
    }, {
      headers: headers
    })
    .then(() => {
      this.setState({message: ""});
    })
  }

  setMessage(event) {
    this.setState({message: event.target.value});
  }

  render() {
    const {message} = this.state

    return (
      <>
        <Header />
        {/* Page content */}
        <Container className="mt--7">
          <Row>
            <Col className="mb-5 mb-xl-0" xl="8">
              <Card className="bg-gradient-default shadow">
                <CardHeader className="bg-transparent">
                  <Row className="align-items-center">
                    <div className="col">
                      <h6 className="text-uppercase text-light ls-1 mb-1">
                        Tweet
                      </h6>
                      <h2 className="text-white mb-0">Whats happening?</h2>
                    </div>
                    <div className="col">
                    </div>
                  </Row>
                </CardHeader>
                <CardBody>
                  <FormGroup>
                    <Input
                      className="form-control-alternative"
                      placeholder="..."
                      rows="4"
                      type="textarea"
                      value={message}
                      onChange={this.setMessage}
                    />
                  </FormGroup>
                  <ul className="justify-content-end nav nav-pills">
                    <li className="nav-item">
                      <button className="btn btn-primary" onClick={this.handleClick}>
                        <span className="font-weight-800">Tweet</span>
                      </button>
                    </li>
                  </ul>
                </CardBody>
              </Card>

              <HomeTweets/>
            </Col>

            <Col xl="4">
              <UserTweets/>
            </Col>

          </Row>


        </Container>
      </>
    )
  }
}

export default withAuth(Home);
