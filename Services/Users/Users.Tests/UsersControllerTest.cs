using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Users.Data;
using Users.Logic;
using Users.Models.DTO;
using Xunit;

namespace Tweets.Tests
{
    public class UsersControllerTest
    {
        private IConfiguration Config;
        private DbContextOptions<UsersDbContext> Options;
        
        public UsersControllerTest()
        {
            var configFile = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Integration"
                ? "appsettings.Integration.json"
                : "appsettings.json";

            Config = new ConfigurationBuilder()
                .AddJsonFile(configFile, optional: true)
                .AddEnvironmentVariables()
                .Build();
            
            Options = new DbContextOptionsBuilder<UsersDbContext>()
                .UseMySql(
                    Config.GetConnectionString("MySql"),
                    new MySqlServerVersion(new Version(8, 0, 21)),
                    mySqlOptions =>
                    {
                        mySqlOptions.EnableRetryOnFailure(
                            maxRetryCount: 2);
                    }).Options;
            
            using (var context = new UsersDbContext(Options))
            {
                context.Database.Migrate();
            }
        }

        [Fact]
        public async void Can_PostTweet_Under140Chars()
        {
            await using (var context = new UsersDbContext(Options))
            {
                var usersRepository = new UsersRepository(context);

               
                // Assert.IsType<long>(tweet.Id);
                Assert.True(true);
            }
        }
    }
}