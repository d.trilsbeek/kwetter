using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Users.Data;
using Users.Models.Entity;
using Users.ServiceBus;

namespace Users.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UsersDbContext _context;
        private readonly KwetterServiceBus _serviceBus;

        public UsersController(
            UsersDbContext context,
            KwetterServiceBus serviceBus
            )
        {
            _context = context;
            _serviceBus = serviceBus;
        }

        // GET: api/Users/dtrilsbeek
        [HttpGet("{username}")]
        public async Task<ActionResult<User>> GetUser(string username)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Username.Equals(username));

            if (user == null)
            {
                return NotFound();
            }
            
            return await Task.FromResult(user);
        }
        
        // GET: api/Users/dtrilsbeek
        [HttpPost("request-deletion")]
        public async Task<ActionResult<User>> RequestDelete()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Identifier.Equals(userId));

            if (user == null)
            {
                Console.WriteLine("User Not Found");
                return NotFound();
            }
            
            var json = JsonConvert.SerializeObject(user);
            var sender = _serviceBus.CreateSender("user.request-deletion");
            
            await sender.SendMessageAsync(new ServiceBusMessage(json));

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
            
            return await Task.FromResult(user);
        }
    }
}
