using Microsoft.EntityFrameworkCore;
using Users.Models.Entity;

#nullable disable

namespace Users.Data
{
	public partial class UsersDbContext : DbContext
	{
		public UsersDbContext(DbContextOptions<UsersDbContext> options)
			: base(options)
		{
		}
		
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			base.OnConfiguring(optionsBuilder);
		}
		
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
		}

		partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

		public DbSet<User> Users { get; set; }
	}
}