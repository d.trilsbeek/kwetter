using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Users.Data;
using Users.Logic.Exceptions;
using Users.Logic.Validators;
using Users.Models.Entity;

namespace Users.Logic
{
    public class UsersRepository
    {
        private readonly UsersDbContext _context;

        public UsersRepository(
            UsersDbContext context
        )
        {
            _context = context;
        }


        public async Task<ActionResult<IEnumerable<User>>> GetUsers()
        {
            return await _context.Users.ToListAsync();
        }

        public async Task<ActionResult<User>> GetUser(long id)
        {
            var user = await _context.Users.FindAsync(id);

            return user;
        }

        public async Task<List<User>> GetUserUsers(string userName)
        {
            var users = await _context.Users.Where(t => t.Username.Equals(userName)).ToListAsync();

            return users;
        }

        public async Task<IActionResult> PutUser(long id, User user)
        {
            if (id != user.Id)
            {
                throw new NotExistsException();
            }

            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }

            return null;
        }

        public async Task<User> Create(User user)
        {
            UserValidator.Validate(user);

            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();

            return user;
        }

        private bool UserExists(long id)
        {
            return _context.Users.Any(e => e.Id == id);
        }
    }
}