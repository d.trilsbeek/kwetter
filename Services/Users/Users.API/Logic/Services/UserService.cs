using Users.Models.Entity;

namespace Users.Logic.Services
{
    public class UserService
    {
        public User NewUser(string name)
        {
            return new()
            {
                Username = name
            };
        }
    }
}