using System;

namespace Users.Logic.Exceptions
{
    public class NotExistsException : Exception
    {
        public NotExistsException() : base()
        {
            
        }
    }
}