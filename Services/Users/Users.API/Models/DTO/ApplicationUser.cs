using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Users.Models.DTO
{
	public class ApplicationUser
	{
		public string Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public string UserName { get; set; }
	}
}