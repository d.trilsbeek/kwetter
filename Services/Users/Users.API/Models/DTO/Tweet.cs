using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Users.Models.DTO
{
    public class Tweet
    {
        public long Id { get; set; }
        public string Message { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string UserId { get; set; }
        public string PictureThumb { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime Inserted { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime LastUpdated { get; set; }
    }
}