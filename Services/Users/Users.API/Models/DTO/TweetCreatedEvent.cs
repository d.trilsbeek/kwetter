namespace Users.Models.DTO
{
	public class TweetCreatedEvent
	{
		public long TweetId { get; set; }
		public string UserId { get; set; }
		public int UserTweetCount { get; set; }
	}
}