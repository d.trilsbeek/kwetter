using System;
using System.Linq;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Users.Data;
using Users.Models.DTO;
using Users.Models.Entity;

namespace Users.ServiceBus
{
    public class MessageHandler
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly KwetterServiceBus _serviceBus;

        public MessageHandler(
            IServiceProvider serviceProvider,
            KwetterServiceBus serviceBus
        )
        {
            _serviceProvider = serviceProvider;
            _serviceBus = serviceBus;
        }

        public async Task HandleCreatedTweet(ProcessMessageEventArgs args)
        {
            try
            {
                var tweetCreatedEvent = args.Message.Body.ToObjectFromJson<TweetCreatedEvent>();

                if (tweetCreatedEvent != null)
                {
                    using (var scope = _serviceProvider.CreateScope())
                    {
                        var context = scope.ServiceProvider.GetRequiredService<UsersDbContext>();

                        var user = await context.Users.FindAsync(tweetCreatedEvent.UserId);

                        if (user != null)
                        {
                            user.TweetCount = tweetCreatedEvent.UserTweetCount;
                            await context.SaveChangesAsync();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }

        public async Task HandleCreateFollow(ProcessMessageEventArgs args)
        {
            try
            {
                var follow = args.Message.Body.ToObjectFromJson<Follow>();

                if (follow != null)
                {
                    using (var scope = _serviceProvider.CreateScope())
                    {
                        var context = scope.ServiceProvider.GetRequiredService<UsersDbContext>();

                        var follower = context.Users.FirstOrDefault(u => u.Username.Equals(follow.Username));
                        var user = context.Users.FirstOrDefault(u => u.Username.Equals(follow.FollowingUsername));

                        if (user != null && follower != null)
                        {
                            follow.Username = follower.Username;
                            follow.FollowingUserId = user.Identifier;

                            var json = JsonConvert.SerializeObject(follow);
                            await args.CompleteMessageAsync(args.Message);
                        
                            var sender = _serviceBus.CreateSender("follow.create.filled");
                            await sender.SendMessageAsync(new ServiceBusMessage(json));

                            Console.WriteLine($"Follow {follow.Username} -> {follow.FollowingUsername} filled");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }

        public async Task HandleCreateUser(ProcessMessageEventArgs args)
        {
            try
            {
                var user = args.Message.Body.ToObjectFromJson<User>();

                if (user != null)
                {
                    using (var scope = _serviceProvider.CreateScope())
                    {
                        var context = scope.ServiceProvider.GetRequiredService<UsersDbContext>();

                        if (context.Users.Any(u => u.Username == user.Username))
                        {
                            throw new Exception("User Exists");
                        }

                        context.Users.Add(user);
                        await context.SaveChangesAsync();

                        var json = JsonConvert.SerializeObject(user);
                        await args.CompleteMessageAsync(args.Message);
                        
                        var sender = _serviceBus.CreateSender("user.created");
                        await sender.SendMessageAsync(new ServiceBusMessage(json));
                    }
                }

                Console.WriteLine($"User {user.Username} created");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task<Task> HandleFillPartialTweet(ProcessMessageEventArgs args)
        {
            try
            {
                string body = args.Message.Body.ToString();
                Console.WriteLine($"Received: {body}");

                var tweet = args.Message.Body.ToObjectFromJson<Tweet>();

                if (tweet != null)
                {
                    using (var scope = _serviceProvider.CreateScope())
                    {
                        var context = scope.ServiceProvider.GetRequiredService<UsersDbContext>();

                        var user = context.Users.FirstOrDefault(u => u.Identifier.Equals(tweet.UserId));

                        if (user != null)
                        {
                            tweet.Username = user.Username;
                            tweet.Name = user.GetFullName();
                            tweet.PictureThumb = user.Picture;
                            
                            await args.CompleteMessageAsync(args.Message);

                            var json = JsonConvert.SerializeObject(tweet);
                            var sender = _serviceBus.CreateSender("tweets.create.filled");
                            await sender.SendMessageAsync(new ServiceBusMessage(json));
                        }
                    }
                }

                Console.WriteLine(tweet?.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return Task.CompletedTask;
        }
    }
}