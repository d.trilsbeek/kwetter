﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using System;
using Elastic.Apm.NetCoreAll;
using Identity.Data;
using Identity.Models;
using Identity.Quickstart;
using Identity.ServiceBus;
using IdentityServer4;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MySqlConnector;

namespace Identity
{
	public class Startup
	{
		public IWebHostEnvironment Environment { get; }
		public IConfiguration Configuration { get; }

		public Startup(IWebHostEnvironment environment, IConfiguration configuration)
		{
			Environment = environment;
			Configuration = configuration;
		}

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddCors(options =>
			{
				options.AddDefaultPolicy(
					builders =>
					{
						builders.WithOrigins(
								"http://localhost",
								"https://localhost",
								"https://kwetter.me",
								"https://www.kwetter.me"
							)
							.AllowAnyHeader()
							.AllowAnyMethod()
							.AllowCredentials();
					});
			});
			services.AddControllersWithViews();

			if (Environment.IsProduction())
			{
				services.Configure<ForwardedHeadersOptions>(options =>
				{
					options.ForwardedHeaders =
						ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
				});
			}
			
			var sqlBuilder = new MySqlConnectionStringBuilder
			{
				Server = "db-mysql-ams3-kwetter-do-user-9307124-0.b.db.ondigitalocean.com",
				Port = 25060,
				Database = "kwetter_identity",
				UserID = "doadmin",
				Password = Configuration.GetConnectionString("MySql"),
				SslMode = MySqlSslMode.Required,
			};

			var connString = Environment.IsProduction()
				? sqlBuilder.ConnectionString
				: Configuration.GetConnectionString("MySql");

			services.AddDbContext<ApplicationDbContext>(options =>
			{
				options.UseMySql(
					connString,
					new MySqlServerVersion(new Version(8, 0, 23)),
					mySqlOptions =>
					{
						mySqlOptions.EnableRetryOnFailure(
							maxRetryCount: 2);
					});
			});
			var rsa = new RsaKeyService(Environment, TimeSpan.FromDays(30));
			services.AddTransient<RsaKeyService>(provider => rsa);
			services.AddIdentity<ApplicationUser, IdentityRole>(options =>
				{
					options.Password.RequireDigit = false;
					options.Password.RequiredLength = 6;
					options.Password.RequireNonAlphanumeric = false;
					options.Password.RequireUppercase = false;
					options.Password.RequireLowercase = false;
					options.Lockout.AllowedForNewUsers = true;
					options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(2);
					options.Lockout.MaxFailedAccessAttempts = 3;
				})
				.AddEntityFrameworkStores<ApplicationDbContext>()
				.AddDefaultTokenProviders()
				.Services.ConfigureApplicationCookie(options =>
				{
					options.Cookie.IsEssential = true;
					// we need to disable to allow iframe for authorize requests
					options.Cookie.SameSite = SameSiteMode.None;
				});

			var builder = services.AddIdentityServer(options =>
				{
					options.Events.RaiseErrorEvents = true;
					options.Events.RaiseInformationEvents = true;
					options.Events.RaiseFailureEvents = true;
					options.Events.RaiseSuccessEvents = true;

					// see https://identityserver4.readthedocs.io/en/latest/topics/resources.html
					options.EmitStaticAudienceClaim = true;
				})
				.AddInMemoryIdentityResources(Config.IdentityResources)
				.AddInMemoryApiScopes(Config.ApiScopes)
				.AddInMemoryClients(Config.Clients)
				.AddAspNetIdentity<ApplicationUser>();


			if (Environment.IsDevelopment())
			{
				builder.AddDeveloperSigningCredential();
			}
			else
			{
				builder.AddSigningCredential(rsa.GetKey(), IdentityServerConstants.RsaSigningAlgorithm.RS512);
			}
			
			// services.AddSession(options => options.Cookie.SameSite = SameSiteMode.None);

			services.AddAuthentication()
				.AddGoogle(options =>
				{
					options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;

					// register your IdentityServer with Google at https://console.developers.google.com
					// enable the Google+ API
					// set the redirect URI to https://localhost:5001/signin-google
					options.ClientId = "copy client ID from Google here";
					options.ClientSecret = "copy client secret from Google here";
				});
			
			services.AddSingleton(o =>
				ActivatorUtilities.CreateInstance<KwetterServiceBus>(o,
					Configuration.GetConnectionString("AzureServiceBusConnection")
				));
			services.AddSingleton<MessageHandler>();
			services.AddSingleton<InitServiceBus>();
			
			services.AddScoped<IProfileService, ProfileService>();


		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env, InitServiceBus serviceBus)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				app.UseDatabaseErrorPage();
			}
			else
			{
				app.UseForwardedHeaders();
				app.Use((context, next) =>
				{
					context.Request.Scheme = "https";
					return next();
				});
			}
			if (env.IsProduction())
			{
				app.UseAllElasticApm(Configuration);
			}

			app.UseCors();

			serviceBus.Configure();
			app.UseStaticFiles();
			app.UseRouting();
			// app.Map("/api/identity", appBuilder => appBuilder.UseIdentityServer());
			app.UseIdentityServer();

			app.UseAuthorization();
			app.UseEndpoints(endpoints => { endpoints.MapDefaultControllerRoute(); });
		}
	}
}