﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using System;
using System.Linq;
using System.Security.Claims;
using Azure.Messaging.ServiceBus;
using Identity.Data;
using Identity.Models;
using Identity.Seed;
using Identity.ServiceBus;
using IdentityModel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MySqlConnector;
using Newtonsoft.Json;
using Serilog;

namespace Identity
{
	public class SeedData
	{
		public async static void EnsureSeedData(IConfiguration config, IWebHostEnvironment env)
		{
			var services = new ServiceCollection();
			services.AddLogging();
			
			var builder = new MySqlConnectionStringBuilder
			{
				Server = "db-mysql-ams3-kwetter-do-user-9307124-0.b.db.ondigitalocean.com",
				Port = 25060,
				Database = "kwetter_identity",
				UserID = "doadmin",
				Password = config.GetConnectionString("MySql"),
				SslMode = MySqlSslMode.Required,
			};
			
			var connString = env.IsProduction()
			? builder.ConnectionString
			: config.GetConnectionString("MySql");
			
			services.AddDbContext<ApplicationDbContext>(options =>
			{
				options.UseMySql(
					connString,
					new MySqlServerVersion(new Version(8, 0, 23)),
					mySqlOptions =>
					{
						mySqlOptions.EnableRetryOnFailure(
							maxRetryCount: 2);
					});
			});

			services.AddIdentity<ApplicationUser, IdentityRole>()
				.AddEntityFrameworkStores<ApplicationDbContext>()
				.AddDefaultTokenProviders();

			services.AddSingleton(o =>
				ActivatorUtilities.CreateInstance<KwetterServiceBus>(o,
					config.GetConnectionString("AzureServiceBusConnection")
				));
			services.AddSingleton<MessageHandler>();
			services.AddSingleton<InitServiceBus>();

			await using (var serviceProvider = services.BuildServiceProvider())
			{
				var serviceBus = serviceProvider.GetRequiredService<KwetterServiceBus>();
				using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
				{
					var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
					await context.Database.MigrateAsync();

					var roleMgr = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
					var user = roleMgr.FindByNameAsync("user").Result;
					if (user == null)
					{
						user = new IdentityRole
						{
							Name = "user",
						};
						_ = roleMgr.CreateAsync(user).Result;
					}

					var moderator = roleMgr.FindByNameAsync("moderator").Result;
					if (moderator == null)
					{
						moderator = new IdentityRole
						{
							Name = "moderator",
						};
						_ = roleMgr.CreateAsync(moderator).Result;
					}

					var admin = roleMgr.FindByNameAsync("admin").Result;
					if (admin == null)
					{
						admin = new IdentityRole
						{
							Name = "admin",
						};
						_ = roleMgr.CreateAsync(admin).Result;
					}

					var userMgr = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
					var alice = userMgr.FindByNameAsync("alice").Result;
					if (alice == null)
					{
						alice = new ApplicationUser
						{
							UserName = "alice",
							FirstName = "Alice",
							LastName = "",
							Email = "alice@umbrella.com",
							EmailConfirmed = true,
						};
						var result = userMgr.CreateAsync(alice, "Pass123$").Result;
						if (!result.Succeeded)
						{
							throw new Exception(result.Errors.First().Description);
						}

						if (!userMgr.IsInRoleAsync(alice, user.Name).Result)
						{
							_ = userMgr.AddToRoleAsync(alice, user.Name).Result;
						}

						result = userMgr.AddClaimsAsync(alice, new Claim[]
						{
							new Claim(JwtClaimTypes.Name, "Alice"),
							new Claim(JwtClaimTypes.GivenName, "Alice"),
							new Claim(JwtClaimTypes.FamilyName, ""),
							new Claim(JwtClaimTypes.WebSite, "http://alice.com"),
						}).Result;

						if (!result.Succeeded)
						{
							throw new Exception(result.Errors.First().Description);
						}

						var aliceUser = JsonConvert.DeserializeObject<User>(JsonConvert.SerializeObject(alice));

						if (aliceUser != null)
						{
							aliceUser.Identifier = alice.Id;
							aliceUser.Bio = "My Name Is Alice... And I Remember Everything.";
							aliceUser.Picture = "https://images-na.ssl-images-amazon.com/images/I/51Jyic8ZK4L._AC_.jpg";
							aliceUser.Location = "Raccoon City";
							aliceUser.University = "The Hive";
							aliceUser.Website = "http://alice.com";

							var sender = serviceBus.CreateSender("users.create");
							sender.SendMessageAsync(new ServiceBusMessage(JsonConvert.SerializeObject(aliceUser))).Wait();

							var tweet = new Tweet
							{
								Message = "Hi, I'm Alice",
								Username = alice.UserName,
								Name = $"{alice.FirstName} {alice.LastName}",
								UserId = alice.Id,
								PictureThumb = "https://images-na.ssl-images-amazon.com/images/I/51Jyic8ZK4L._AC_.jpg",
							};
							var json = JsonConvert.SerializeObject(tweet);
							
							var sender2 = serviceBus.CreateSender("tweets.create.filled");
							await sender2.SendMessageAsync(new ServiceBusMessage(json));

							Log.Debug("alice created");
						}
					}
					else
					{
						Log.Debug("alice already exists");
					}

					var bob = userMgr.FindByNameAsync("bob_ross").Result;
					if (bob == null)
					{
						bob = new ApplicationUser
						{
							UserName = "bob_ross",
							FirstName = "Bob",
							LastName = "Ross",
							Email = "Bob@Ross.com",
							EmailConfirmed = true
						};

						var result = userMgr.CreateAsync(bob, "Pass123$").Result;
						if (!result.Succeeded)
						{
							throw new Exception(result.Errors.First().Description);
						}

						if (!userMgr.IsInRoleAsync(bob, user.Name).Result)
						{
							_ = userMgr.AddToRoleAsync(bob, user.Name).Result;
						}

						result = userMgr.AddClaimsAsync(bob, new Claim[]
						{
							new Claim(JwtClaimTypes.Name, "Bob Ross"),
							new Claim(JwtClaimTypes.GivenName, "Bob"),
							new Claim(JwtClaimTypes.FamilyName, "Ross"),
							new Claim(JwtClaimTypes.WebSite, "http://bob.com"),
							new Claim("location", "somewhere"),
						}).Result;
						if (!result.Succeeded)
						{
							throw new Exception(result.Errors.First().Description);
						}

						var bobUser = JsonConvert.DeserializeObject<User>(JsonConvert.SerializeObject(bob));

						if (bobUser != null)
						{
							bobUser.Identifier = bob.Id;
							bobUser.Bio = "Ross was born Robert Norman Ross in Daytona, Florida, on October 29, 1942. He was raised in Orlando, Florida. After dropping out of school in the ninth grade, Ross served in the U.S. Air Force. During his service, he took his first painting lesson at an Anchorage, Alaska United Service Organizations club. From that point on, he was hooked, a term he would use frequently during his years as a painting instructor.";
							bobUser.Picture = "https://pbs.twimg.com/profile_images/1276672591000858624/ofqysR1Y_400x400.jpg";
							bobUser.Location = "Daytona Beach, Florida";
							bobUser.University = "";
							bobUser.Website = "http://alice.com";

							var sender = serviceBus.CreateSender("users.create");
							sender.SendMessageAsync(new ServiceBusMessage(JsonConvert.SerializeObject(bobUser))).Wait();

							foreach (var saying in Bob.Sayings)
							{
								var tweet = new Tweet
								{
									Message = saying,
									Username = bob.UserName,
									Name = $"{bob.FirstName} {bob.LastName}",
									UserId = bob.Id,
									PictureThumb =
										"https://pbs.twimg.com/profile_images/1276672591000858624/ofqysR1Y_400x400.jpg",
								};

								var sender2 = serviceBus.CreateSender("tweets.create.filled");
								await sender2.SendMessageAsync(new ServiceBusMessage(JsonConvert.SerializeObject(tweet)));
							}

							Log.Debug("bob created");
						}
					}
					else
					{
						Log.Debug("bob already exists");
					}
				}
			}
		}
	}
}