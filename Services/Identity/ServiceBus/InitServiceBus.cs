using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;

namespace Identity.ServiceBus
{
    public class InitServiceBus
    {
        private MessageHandler _messageHandler;
        private readonly KwetterServiceBus _serviceBus;
        private List<ServiceBusProcessor> _processors;

        public InitServiceBus(
            MessageHandler messageHandler,
            KwetterServiceBus serviceBus
            )
        {
            _messageHandler = messageHandler;
            _serviceBus = serviceBus;
            _processors = new List<ServiceBusProcessor>();
        }

        private void InitProcessors()
        {
            CreateProcessor("user.request-deletion", "identity", _messageHandler.HandeRequestDeletion);
        }

        private void CreateProcessor(string queueName, Func<ProcessMessageEventArgs, Task> handler)
        {
            _serviceBus.CreateQueue(queueName).Wait();
            var processor = _serviceBus.CreateProcessor(queueName, new ServiceBusProcessorOptions());
            processor.ProcessMessageAsync += handler;
            processor.ProcessErrorAsync += ErrorHandler;

            _processors.Add(processor);
        }
        
        private void CreateProcessor(string topicName, string subscriptionName, Func<ProcessMessageEventArgs, Task> handler)
        {
            var processor = _serviceBus.CreateProcessor(topicName, subscriptionName, new ServiceBusProcessorOptions());
            processor.ProcessMessageAsync += handler;
            processor.ProcessErrorAsync += ErrorHandler;

            _processors.Add(processor);
        }


        private Task ErrorHandler(ProcessErrorEventArgs args)
        {
            Console.WriteLine(args.Exception.ToString());
            return Task.CompletedTask;
        }


        public async void Configure()
        {
            InitProcessors();
            foreach (var processor in _processors)
            {
                await processor.StartProcessingAsync();
            }
        }
    }
}