using System;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using Identity.Data;
using Identity.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace Identity.ServiceBus
{
    public class MessageHandler
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly KwetterServiceBus _serviceBus;

        public MessageHandler(
            IServiceProvider serviceProvider,
            KwetterServiceBus serviceBus
        )
        {
            _serviceProvider = serviceProvider;
            _serviceBus = serviceBus;
        }
        
        public async Task<Task> HandeRequestDeletion(ProcessMessageEventArgs args)
        {
            try
            {
                var user = args.Message.Body.ToObjectFromJson<User>();

                Console.WriteLine("Request User Deletion");
                Console.WriteLine(user.Username);
                
                    using (var scope = _serviceProvider.CreateScope())
                    {
                        var userManager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                        var applicationUser = await userManager.FindByIdAsync(user.Identifier);
                        var result = await userManager.DeleteAsync(applicationUser);
                        Console.WriteLine(result);
                    }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
            return Task.CompletedTask;
        }
    }
}