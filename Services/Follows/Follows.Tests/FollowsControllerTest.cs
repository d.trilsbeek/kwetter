using System;
using Microsoft.Extensions.Configuration;
using Xunit;
using Xunit.Abstractions;

namespace Follows.Tests
{
    public class FollowsControllerTest
    {
        private IConfiguration Config;
        
        public FollowsControllerTest()
        {
            var configFile = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Integration"
                ? "appsettings.Integration.json"
                : "appsettings.json";

            Config = new ConfigurationBuilder()
                .AddJsonFile(configFile, optional: true)
                .AddEnvironmentVariables()
                .Build();

        }

        [Fact]
        public void CheckConnectionString()
        {
            var connectionString = Config.GetConnectionString("AzureServiceBusConnection").Substring(0, 8);
            
            Assert.Equal("Endpoint", connectionString);
        }
        
        // test with existing user
        // test with non existing user
    }
}
