using Follows.Models.Entity;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Follows.Data
{
	public partial class FollowDbContext : DbContext
	{
		public FollowDbContext(DbContextOptions<FollowDbContext> options)
			: base(options)
		{
		}
		
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			base.OnConfiguring(optionsBuilder);
		}
		
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
		}

		partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

		public DbSet<Follow> Follows { get; set; }
	}
}