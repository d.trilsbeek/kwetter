namespace Follows.Models.DTO
{
    public class UserFollows
    {
        public string Username { get; set; }
        public int Followers { get; set; }
        public int Following { get; set; }
    }
}