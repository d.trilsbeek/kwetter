namespace Follows.Models.Entity
{
    public class Follow
    {
        public long Id { get; set; }
        public string UserId { get; set; }
        public string Username { get; set; }
        
        public string FollowingUserId { get; set; }
        public string FollowingUsername { get; set; }
    }
}