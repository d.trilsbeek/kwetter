using System.Security.Claims;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using Follows.Data;
using Follows.Helpers;
using Follows.Models.DTO;
using Follows.Models.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Tweets.ServiceBus;

namespace Follows.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FollowsController : ControllerBase
    {
        private readonly FollowDbContext _context;
        private readonly KwetterServiceBus _serviceBus;

        public FollowsController(
            FollowDbContext context,
                KwetterServiceBus serviceBus
            )
        {
            _context = context;
            _serviceBus = serviceBus;
        }
        
        [HttpGet("users/{username}")]
        public async Task<UserFollows> GetFollows(string username)
        {
            var followers = await _context.Follows.CountAsync(f => f.FollowingUsername.Equals(username));
            var following = await _context.Follows.CountAsync(f => f.Username.Equals(username));

            var userFollows = new UserFollows
            {
                Username = username,
                Followers = followers,
                Following = following
            };

            return userFollows;
        }
        
        [HttpPost]
        [Authorize(Roles = RolesHelper.AllowAll)]
        public async Task<ActionResult<Follow>> Follow(Follow follow)
        {
            follow.UserId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            await _context.Follows.AddAsync(follow);
            await _context.SaveChangesAsync();
            
            var json = JsonConvert.SerializeObject(follow);
            
            var sender = _serviceBus.CreateSender("follow.create.partial");
            await sender.SendMessageAsync(new ServiceBusMessage(json));
            
            return CreatedAtAction("Follow", new { id = follow.Id }, follow);
        }

    }
}
