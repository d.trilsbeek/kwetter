using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using Follows.ServiceBus;
using Tweets.ServiceBus;

namespace Users.ServiceBus
{
    public class InitServiceBus
    {
        private MessageHandler _messageHandler;
        private readonly KwetterServiceBus _serviceBus;
        private List<ServiceBusProcessor> _processors;

        public InitServiceBus(
            MessageHandler messageHandler,
            KwetterServiceBus serviceBus
            )
        {
            _messageHandler = messageHandler;
            _serviceBus = serviceBus;
            _processors = new List<ServiceBusProcessor>();
        }
        private async void InitQueues()
        {
            await _serviceBus.CreateQueue("follow.created");
        }
        private void InitProcessors()
        {
            CreateProcessor("follow.create.filled", _messageHandler.HandleCreateFollow);
            CreateProcessor("user.request-deletion", "follows-api", _messageHandler.HandeRequestDeletion);
        }

        private void CreateProcessor(string queueName, Func<ProcessMessageEventArgs, Task> handler)
        {
            _serviceBus.CreateQueue(queueName).Wait();
            var processor = _serviceBus.CreateProcessor(queueName, new ServiceBusProcessorOptions());
            processor.ProcessMessageAsync += handler;
            processor.ProcessErrorAsync += ErrorHandler;

            _processors.Add(processor);
        }
        
        private void CreateProcessor(string topicName, string subscriptionName, Func<ProcessMessageEventArgs, Task> handler)
        {
            var processor = _serviceBus.CreateProcessor(topicName, subscriptionName, new ServiceBusProcessorOptions());
            processor.ProcessMessageAsync += handler;
            processor.ProcessErrorAsync += ErrorHandler;

            _processors.Add(processor);
        }



        private Task ErrorHandler(ProcessErrorEventArgs args)
        {
            Console.WriteLine(args.Exception.ToString());
            return Task.CompletedTask;
        }


        public async void Configure()
        {
            InitQueues();
            InitProcessors();
            foreach (var processor in _processors)
            {
                await processor.StartProcessingAsync();
            }
        }
    }
}