using System;
using System.Linq;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using Follows.Data;
using Follows.Models.Entity;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Tweets.Models.Entity;
using Tweets.ServiceBus;

namespace Follows.ServiceBus
{
    public class MessageHandler
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly KwetterServiceBus _serviceBus;

        public MessageHandler(
            IServiceProvider serviceProvider,
            KwetterServiceBus serviceBus
            )
        {
            _serviceProvider = serviceProvider;
            _serviceBus = serviceBus;
        }
        
        public async Task<Task> HandeRequestDeletion(ProcessMessageEventArgs args)
        {
            try
            {
                var user = args.Message.Body.ToObjectFromJson<User>();
                
                Console.WriteLine("Request User Deletion");
                Console.WriteLine(user.Username);

                using (var scope = _serviceProvider.CreateScope())
                {
                    var context = scope.ServiceProvider.GetRequiredService<FollowDbContext>();
                    
                    context.Follows.RemoveRange(
                        context.Follows.Where(t => t.UserId.Equals(user.Identifier))
                    );

                    await context.SaveChangesAsync();

                    Console.WriteLine("User follows deleted");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
            return Task.CompletedTask;
        }
        
        public async Task<Task> HandleCreateFollow(ProcessMessageEventArgs args)
        {
            try
            {
                var follower = args.Message.Body.ToObjectFromJson<Follow>();
                
                if (follower != null)
                {
                    using (var scope = _serviceProvider.CreateScope())
                    {
                        var context = scope.ServiceProvider.GetRequiredService<FollowDbContext>();

                        context.Follows.Update(follower);
                        await context.SaveChangesAsync();

                        var json = JsonConvert.SerializeObject(follower);
                        
                        var sender = _serviceBus.CreateSender("follow.created");
                        await sender.SendMessageAsync(new ServiceBusMessage(json));
                        
                        Console.WriteLine($"Follower {follower.FollowingUsername} created");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
            return Task.CompletedTask;
        }
    }
}