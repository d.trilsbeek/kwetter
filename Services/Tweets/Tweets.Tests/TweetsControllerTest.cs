using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Tweets.Data;
using Tweets.Logic;
using Tweets.Logic.Exceptions;
using Tweets.Models.Entity;
using Xunit;

namespace Tweets.Tests
{
    public class TweetsControllerTest
    {
        private IConfiguration Config;
        private DbContextOptions<TweetsDbContext> Options;
        
        public TweetsControllerTest()
        {
            var configFile = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Integration"
                ? "appsettings.Integration.json"
                : "appsettings.json";

            Config = new ConfigurationBuilder()
                .AddJsonFile(configFile, optional: true)
                .AddEnvironmentVariables()
                .Build();
            
            Options = new DbContextOptionsBuilder<TweetsDbContext>()
                .UseMySql(
                    Config.GetConnectionString("MySql"),
                    new MySqlServerVersion(new Version(8, 0, 21)),
                    mySqlOptions =>
                    {
                        mySqlOptions.EnableRetryOnFailure(
                            maxRetryCount: 2);
                    }).Options;
            
            using (var context = new TweetsDbContext(Options))
            {
                context.Database.Migrate();
            }
        }

        [Fact]
        public async void Can_PostTweet_Under140Chars()
        {
            await using (var context = new TweetsDbContext(Options))
            {
                var tweetsRepository = new TweetsRepository(context);

                var tweet = new Tweet
                {
                    Username = "dtrilsbeek",
                    Name = "Dion Trilsbeek",
                    Message = "Hello Kwetter",
                    
                };

                var result = await tweetsRepository.Create(tweet);

                Assert.Equal(tweet.Message, result.Message);
                Assert.Equal(tweet.Username, result.Username);
                Assert.Equal(tweet.Name, result.Name);
                Assert.IsType<long>(tweet.Id);
            }
        }

        [Fact]
        public async void Can_PostTweet_Over140Chars_ThrowException()
        {
            await using (var context = new TweetsDbContext(Options))
            {
                var tweetsRepository = new TweetsRepository(context);

                var tweet = new Tweet
                {
                    Username = "dtrilsbeek",
                    Name = "Dion Trilsbeek",
                    Message = "Hello Kwetter, this is a tweet that is too long. " +
                    "Hello Kwetter, this is a tweet that is too long." +
                    "Hello Kwetter, this is a tweet that is too long." +
                    "Hello Kwetter, this is a tweet that is too long."
                };

                await Assert.ThrowsAsync<TweetMessageTooLongException>(() => tweetsRepository.Create(tweet));
            }
        }
    }
}