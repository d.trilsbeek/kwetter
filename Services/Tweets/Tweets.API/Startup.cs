using System;
using System.Net.Http;
using Elastic.Apm.NetCoreAll;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using MySqlConnector;
using Tweets.Data;
using Tweets.Helpers;
using Tweets.Logic;
using Tweets.ServiceBus;
using Users.ServiceBus;

namespace Tweets
{
    public class Startup
    {
        private readonly IWebHostEnvironment _env;

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            _env = env;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var sqlBuilder = new MySqlConnectionStringBuilder
            {
                Server = "db-mysql-ams3-kwetter-do-user-9307124-0.b.db.ondigitalocean.com",
                Port = 25060,
                Database = "kwetter_tweets",
                UserID = "doadmin",
                Password = Configuration.GetConnectionString("MySql"),
                SslMode = MySqlSslMode.Required,
            };

            var connString = _env.IsProduction()
                ? sqlBuilder.ConnectionString
                : Configuration.GetConnectionString("MySql");
            
            services.AddDbContext<TweetsDbContext>(options =>
            {
                options.UseMySql(
                    connString,
                    new MySqlServerVersion(new Version(8, 0, 23)),
                    mySqlOptions =>
                    {
                        mySqlOptions.EnableRetryOnFailure(
                            maxRetryCount: 2);
                    });
            });

            services.Configure<RouteOptions>(o => o.LowercaseUrls = true);
            services.AddCors(options =>
            {
                options.AddPolicy(name: MyAllowSpecificOrigins,
                    builder =>
                    {
                        builder.WithOrigins("http://localhost")
                            .AllowAnyHeader()
                            .AllowAnyHeader()
                            .AllowCredentials();
                    });
            });
            services.AddControllers(o =>
            {
                o.Conventions.Add(new RouteTokenTransformerConvention(new SlugifyParameterTransformer()));
            });

            services.AddAuthentication("Bearer")
                .AddJwtBearer("Bearer", options =>
                {
                    options.Authority = Configuration.GetValue<string>("Authority");

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateAudience = false
                    };
                });
            
            services.AddHttpClient<HttpClient>();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "Tweets.API", Version = "v1"});
            });
            
            services.AddSingleton(o =>
                ActivatorUtilities.CreateInstance<KwetterServiceBus>(o,
                    Configuration.GetConnectionString("AzureServiceBusConnection")
                ));
            services.AddSingleton<MessageHandler>();
            services.AddSingleton<InitServiceBus>();
            
            services.AddTransient<TweetsRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, InitServiceBus serviceBus)
        {
           
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Users.API v1"));
            }

            if (env.IsProduction())
            {
                app.UseAllElasticApm(Configuration);
            }

            using (var scope = app.ApplicationServices.CreateScope())
            {
                using (var context = scope.ServiceProvider.GetRequiredService<TweetsDbContext>())
                {
                    context.Database.Migrate();
                }
            }
            
            serviceBus.Configure();

            app.UseRouting();

            app.UseCors(MyAllowSpecificOrigins);

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}