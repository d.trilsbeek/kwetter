namespace Tweets.Helpers
{
    public static class RolesHelper
    {
        public const string AllowAll = "user,moderator,admin";
        public const string AllowModerator  = "moderator,admin";
        public const string AllowAdmin = "admin";
    }
}