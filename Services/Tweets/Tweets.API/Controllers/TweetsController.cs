using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Tweets.Data;
using Tweets.Helpers;
using Tweets.Logic;
using Tweets.Models.Entity;
using Tweets.ServiceBus;

namespace Tweets.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TweetsController : ControllerBase
    {
        private readonly TweetsDbContext _context;
        private readonly TweetsRepository _tweetsRepository;
        private readonly KwetterServiceBus _serviceBus;

        public TweetsController(
            TweetsDbContext context,
            TweetsRepository tweetsRepository,
            KwetterServiceBus serviceBus
        )
        {
            _context = context;
            _tweetsRepository = tweetsRepository;
            _serviceBus = serviceBus;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Tweet>>> GetTweets()
        {
            return await _context.Tweets.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Tweet>> GetTweet(long id)
        {
            var tweet = await _context.Tweets.FindAsync(id);

            if (tweet == null)
            {
                return NotFound();
            }

            return tweet;
        }
        
        [HttpGet("users/{userName}")]
        public async Task<List<Tweet>> GetUserTweets(string userName)
        {
            var tweets = await _context.Tweets.Where(t => t.Username.Equals(userName)).ToListAsync();
            
            return tweets;
        }

        [HttpPut("{id}")]
        [Authorize(Roles = RolesHelper.AllowAll)]
        public async Task<IActionResult> PutTweet(long id, Tweet tweet)
        {
            if (id != tweet.Id)
            {
                return BadRequest();
            }

            _context.Entry(tweet).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TweetExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        [Authorize(Roles = RolesHelper.AllowAll)]
        public async Task<ActionResult<Tweet>> PostTweet(Tweet tweet)
        {
            tweet.UserId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            await _tweetsRepository.Create(tweet);
            
            var json = JsonConvert.SerializeObject(tweet);
            var sender = _serviceBus.CreateSender("tweets.create.partial");

            await sender.SendMessageAsync(new ServiceBusMessage(json));

            return CreatedAtAction("GetTweet", new { id = tweet.Id }, tweet);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = RolesHelper.AllowAll)]
        public async Task<IActionResult> DeleteTweet(long id)
        {
            // TODO: check for user id
            var tweet = await _context.Tweets.FindAsync(id);
            if (tweet == null)
            {
                return NotFound();
            }

            _context.Tweets.Remove(tweet);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool TweetExists(long id)
        {
            return _context.Tweets.Any(e => e.Id == id);
        }
    }
}
