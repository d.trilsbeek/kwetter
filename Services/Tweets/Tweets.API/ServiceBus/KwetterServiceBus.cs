using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using Azure.Messaging.ServiceBus.Administration;

namespace Tweets.ServiceBus
{
    public class KwetterServiceBus : ServiceBusClient
    {
        private readonly ServiceBusAdministrationClient _adminClient;

        public KwetterServiceBus(string connection
        ) : base(connection)
        {
            _adminClient = new ServiceBusAdministrationClient(connection);
        }

        public async Task CreateQueue(string queueName)
        {
            if (!(await _adminClient.QueueExistsAsync(queueName)))
            {
                await _adminClient.CreateQueueAsync(queueName);
            }
        }

        public async Task<ServiceBusSender> CreateQueueAndSender(string queueName)
        {
            await CreateQueue(queueName);
            return CreateSender(queueName);
        }
    }
}