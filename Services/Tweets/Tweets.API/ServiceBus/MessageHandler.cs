using System;
using System.Linq;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Tweets.Data;
using Tweets.Models.DTO;
using Tweets.Models.Entity;

namespace Tweets.ServiceBus
{
    public class MessageHandler
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly KwetterServiceBus _serviceBus;

        public MessageHandler(
            IServiceProvider serviceProvider,
            KwetterServiceBus serviceBus
        )
        {
            _serviceProvider = serviceProvider;
            _serviceBus = serviceBus;
        }

        public async Task<Task> HandleCreateFilledTweet(ProcessMessageEventArgs args)
        {
            try
            {
                string body = args.Message.Body.ToString();
                Console.WriteLine($"Received: {body}");

                var tweet = args.Message.Body.ToObjectFromJson<Tweet>();

                if (tweet != null)
                {
                    using (var scope = _serviceProvider.CreateScope())
                    {
                        var context = scope.ServiceProvider.GetRequiredService<TweetsDbContext>();

                        if (context.Tweets.Any(t => t.Id.Equals(tweet.Id)))
                        {
                            context.Tweets.Update(tweet);
                        }
                        else
                        {
                            context.Tweets.Add(tweet);
                        }

                        await context.SaveChangesAsync();

                        var tweetCount = context.Tweets.Count(t => t.UserId.Equals(tweet.UserId));

                        var tweetCreatedEvent = new TweetCreatedEvent
                        {
                            TweetId = tweet.Id,
                            UserId = tweet.UserId,
                            UserTweetCount = tweetCount
                        };

                        var json = JsonConvert.SerializeObject(tweetCreatedEvent);


                        var sender = _serviceBus.CreateSender("tweet.created");
                        await sender.SendMessageAsync(new ServiceBusMessage(json));
                    }
                }

                Console.WriteLine(tweet?.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return Task.CompletedTask;
        }

        public async Task<Task> HandeRequestDeletion(ProcessMessageEventArgs args)
        {
            try
            {
                var user = args.Message.Body.ToObjectFromJson<User>();
                
                Console.WriteLine("Request User Deletion");
                Console.WriteLine(user.Username);

                using (var scope = _serviceProvider.CreateScope())
                {
                    var context = scope.ServiceProvider.GetRequiredService<TweetsDbContext>();
                    
                    context.Tweets.RemoveRange(
                        context.Tweets.Where(t => t.UserId.Equals(user.Identifier))
                    );

                    await context.SaveChangesAsync();

                    Console.WriteLine("User tweets deleted");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return Task.CompletedTask;
        }
    }
}