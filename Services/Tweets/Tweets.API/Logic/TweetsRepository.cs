using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tweets.Data;
using Tweets.Logic.Exceptions;
using Tweets.Logic.Validators;
using Tweets.Models.Entity;

namespace Tweets.Logic
{
    public class TweetsRepository
    {
        private readonly TweetsDbContext _context;

        public TweetsRepository(
            TweetsDbContext context
        )
        {
            _context = context;
        }


        public async Task<ActionResult<IEnumerable<Tweet>>> GetTweets()
        {
            return await _context.Tweets.ToListAsync();
        }

        public async Task<ActionResult<Tweet>> GetTweet(long id)
        {
            var tweet = await _context.Tweets.FindAsync(id);

            return tweet;
        }

        public async Task<List<Tweet>> GetUserTweets(string userName)
        {
            var tweets = await _context.Tweets.Where(t => t.Username.Equals(userName)).ToListAsync();

            return tweets;
        }

        public async Task<IActionResult> PutTweet(long id, Tweet tweet)
        {
            if (id != tweet.Id)
            {
                throw new NotExistsException();
            }

            _context.Entry(tweet).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TweetExists(id))
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }

            return null;
        }

        public async Task<Tweet> Create(Tweet tweet)
        {
            tweet = await TweetValidator.Validate(tweet);

            await _context.Tweets.AddAsync(tweet);
            await _context.SaveChangesAsync();

            return tweet;
        }

        public async Task<int> DeleteTweet(long id, string userId)
        {
            var tweet = await _context.Tweets.FindAsync(id);
            if (tweet == null)
            {
                throw new NotExistsException();
            }

            if (tweet.UserId != userId)
            {
                throw new NotAuthorizedException();
            }

            _context.Tweets.Remove(tweet);
            return await _context.SaveChangesAsync();
        }

        private bool TweetExists(long id)
        {
            return _context.Tweets.Any(e => e.Id == id);
        }
    }
}