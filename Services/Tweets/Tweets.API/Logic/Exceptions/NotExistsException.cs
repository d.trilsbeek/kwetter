using System;

namespace Tweets.Logic.Exceptions
{
    public class NotExistsException : Exception
    {
        public NotExistsException() : base()
        {
            
        }
    }
}