using System;

namespace Tweets.Logic.Exceptions
{
    public class TweetMessageTooLongException : TweetValidationException
    {
    }
}