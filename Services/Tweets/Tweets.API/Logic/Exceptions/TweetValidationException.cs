using System;

namespace Tweets.Logic.Exceptions
{
    public class TweetValidationException : Exception
    {
        
        public TweetValidationException() : base()
        {
            
        }
        
        public TweetValidationException(string message) : base(message)
        {
            
        }
    }
}