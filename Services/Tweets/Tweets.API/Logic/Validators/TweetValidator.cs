using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Tweets.Logic.Exceptions;
using Tweets.Models.Entity;

namespace Tweets.Logic.Validators
{
    public static class TweetValidator
    {
        public static async Task<Tweet> Validate(Tweet tweet)
        {
            MessageLength(tweet);
            tweet = await DetectSwearWords(tweet);

            return tweet;
        }

        private static bool MessageLength(Tweet tweet)
        {
            if (tweet.Message.Length > 140)
            {
                throw new TweetMessageTooLongException();
            }

            return true;
        }

        private static async Task<Tweet> DetectSwearWords(Tweet tweet)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://europe-west1-kwetter-316218.cloudfunctions.net");
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("message", tweet.Message)
                });
                
                var result = await client.PostAsync("/kwetter-scheldwoord-detector", content);
                string message = await result.Content.ReadAsStringAsync();
                Console.WriteLine(message);

                tweet.Message = message;
            }

            return tweet;
        }
    }
}