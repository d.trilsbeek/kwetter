using Tweets.Cache.Logic.Services;
using Xunit;

namespace Tweets.Cache.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void PostTweet_Under140Chars_ReturnTrue()
        {
            var tweetService = new TweetService();

            var message = "Hello Kwetter";
            var tweet = tweetService.NewTweet(message);
            
            Assert.Equal(message, tweet.Message);
        }
    }
}
