using Microsoft.EntityFrameworkCore;
using Tweets.Cache.Models.Entity;

#nullable disable

namespace Tweets.Cache.Data
{
	public partial class TweetsDbContext : DbContext
	{
		public TweetsDbContext(DbContextOptions<TweetsDbContext> options)
			: base(options)
		{
		}
		
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			base.OnConfiguring(optionsBuilder);
		}
		
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
		}

		partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

		public DbSet<Tweet> Tweets { get; set; }
	}
}