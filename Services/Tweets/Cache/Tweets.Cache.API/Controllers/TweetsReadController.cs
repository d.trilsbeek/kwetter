using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RabbitMQ_Eventbus.Eventbus;
using RabbitMQ_Eventbus.Message;
using Tweets.Cache.Data;
using Tweets.Cache.Models.Entity;

namespace Tweets.Cache.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TweetsReadController : ControllerBase
    {
        private readonly TweetsDbContext _context;
        private readonly IRabbitMQEventbus _eventbus;

        public TweetsReadController(
            TweetsDbContext context,
            IRabbitMQEventbus eventbus
            )
        {
            _context = context;
            _eventbus = eventbus;
        }

        // GET: api/Tweets
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Tweet>>> GetTweet()
        {
            return await _context.Tweets.ToListAsync();
        }

        // GET: api/Tweets/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Tweet>> GetTweet(long id)
        {
            var tweet = await _context.Tweets.FindAsync(id);

            if (tweet == null)
            {
                return NotFound();
            }

            return tweet;
        }
        
    }
}
