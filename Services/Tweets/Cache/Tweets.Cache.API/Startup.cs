using System;
using System.Net.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using RabbitMQ_Eventbus.Configuration;
using RabbitMQ_Eventbus.DependencyInjection;
using RabbitMQ_Eventbus.FunctionProvider;
using Tweets.Cache.Data;
using Tweets.Cache.Helpers;
using Tweets.Cache.RabbitMQEventbus;

namespace Tweets
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<TweetsDbContext>(options =>
            {
                options.UseMySql(
                    Configuration.GetConnectionString("MySql"),
                    new MySqlServerVersion(new Version(8, 0, 21)),
                mySqlOptions =>
                {
                    mySqlOptions.EnableRetryOnFailure(
                        maxRetryCount: 2);
                });
            });
            
            services.Configure<RouteOptions>(o => o.LowercaseUrls = true);
            services.AddCors(options =>
            {
                options.AddPolicy(name: MyAllowSpecificOrigins,
                    builder =>
                    {
                        builder.WithOrigins("http://localhost")
                            .AllowAnyHeader()
                            .AllowAnyHeader()
                            .AllowCredentials();
                    });
            });
            services.AddControllers(o =>
            {
                o.Conventions.Add(new RouteTokenTransformerConvention(new SlugifyParameterTransformer()));
            });
            services.AddHttpClient<HttpClient>();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Users.API", Version = "v1" });
            });
            
            
            // services.AddTransient<ISearch, Search>();
            services.Configure<RabbitMQEventbusConfiguration>(Configuration);
            services.AddOptions<RabbitMQEventbusConfiguration>();
            services.AddSingleton<IRabbitMQFunctionProvider>(s =>
            {
                return new FunctionProvider(
                    s.GetService<ILogger<FunctionProvider>>()
                );
            });
            services.AddRabbitMQEventbus();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Users.API v1"));
                // SeedDb();
            }
            
            app.UseRabbitMQEventBus();

            app.UseRouting();
            
            app.UseCors(MyAllowSpecificOrigins);

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
