## Scaffold (PostgreSQL)
``` dotnet ef dbcontext scaffold "Host=localhost;Database=database;Username=username;Password=password" Npgsql.EntityFrameworkCore.PostgreSQL --context DbContext --context-dir Data --output-dir Models```

## Connect to local DB
``` 
docker exec -it container_mssql_1 /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P MyPassword001
```

## Create DB
```
CREATE DATABASE kwetter_service;

USE kwetter_service;
GRANT ALL ON *.* TO 'kwetter'@'%';
FLUSH PRIVILEGES;
```


## Scaffold API Controller from model
```
dotnet aspnet-codegenerator controller -name TweetsController -async -api -m Tweet -dc TweetsDbContext -outDir Controllers
```

## Update EF Core from code
```
dotnet ef database update
```

## Add Migration
```
dotnet ef migrations add InitialCreate
```