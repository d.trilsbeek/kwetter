﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client.Events;
using RabbitMQ_Eventbus.FunctionProvider;
using RabbitMQ_Eventbus.Message;
using Tweets.Cache.Models.Entity;

namespace Tweets.Cache.RabbitMQEventbus
{
    public class FunctionProvider : RabbitMQFunctionProviderBase
    {
        private readonly ILogger<FunctionProvider> _logger;

        public FunctionProvider(ILogger<FunctionProvider> logger)
        {
            _logger = logger;
        }

        protected override Dictionary<string, Func<object, BasicDeliverEventArgs, RabbitMQMessage>> InializeFunctionsWithKeys()
        {
            var dictionary = new Dictionary<string, Func<object, BasicDeliverEventArgs, RabbitMQMessage>>();
            dictionary.Add(nameof(HandleTweetCreated), HandleTweetCreated);
            
            return dictionary;
        }

        private RabbitMQMessage HandleTweetCreated(object o, BasicDeliverEventArgs eventArgs)
        {
            var body = eventArgs.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);

            try
            {
                var tweet = Newtonsoft.Json.JsonConvert.DeserializeObject<Tweet>(message);

                Console.WriteLine(tweet?.Message);
            }
            catch (Exception e)
            {
                _logger.LogWarning(e.StackTrace);
            }
            
            return null;
        }
    }
}
