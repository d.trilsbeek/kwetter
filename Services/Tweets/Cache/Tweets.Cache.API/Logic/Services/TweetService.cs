using Tweets.Cache.Models.Entity;

namespace Tweets.Cache.Logic.Services
{
    public class TweetService
    {
        public Tweet NewTweet(string message)
        {
            return new()
            {
                Message = message
            };
        }
    }
}