using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tweets.Cache.Models.Entity
{
    public class User
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }

        public string Bio { get; set; }
        public string Location { get; set; }
        public string Website { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime Inserted { get; set; }
        
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime LastUpdated { get; set; }
    }
}