using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tweets.Cache.Models.Entity
{
    public class Tweet
    {
        public long Id { get; set; }
        public string Message { get; set; }
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime Inserted { get; set; }
        
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime LastUpdated { get; set; }
    }
}