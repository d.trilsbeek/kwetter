CREATE DATABASE kwetter_follows;

USE kwetter_follows;
GRANT ALL ON *.* TO 'kwetter'@'%';
FLUSH PRIVILEGES;

CREATE DATABASE kwetter_identity;

USE kwetter_identity;
GRANT ALL ON *.* TO 'kwetter'@'%';
FLUSH PRIVILEGES;

CREATE DATABASE kwetter_tweets;

USE kwetter_tweets;
GRANT ALL ON *.* TO 'kwetter'@'%';
FLUSH PRIVILEGES;

CREATE DATABASE kwetter_users;

USE kwetter_users;
GRANT ALL ON *.* TO 'kwetter'@'%';
FLUSH PRIVILEGES;
