https://kubernetes.io/docs/tasks/configmap-secret/managing-secret-using-kubectl/
```
kubectl create secret generic azure-service-bus --from-file=./k8s/deployments/secrets/azure-service-bus.txt

kubectl create secret generic azure-db `
    --from-file=./k8s/deployments/secrets/azure-db-follows.txt `
    --from-file=./k8s/deployments/secrets/azure-db-identity.txt `
    --from-file=./k8s/deployments/secrets/azure-db-tweets.txt `
    --from-file=./k8s/deployments/secrets/azure-db-users.txt
    
kubectl create secret generic azure-db `
    --from-file=./k8s/deployments/secrets/azure-db.txt `
    
kubectl create secret generic digitalocean-db --from-literal=password=''

kubectl create secret generic elastic-apm --from-literal=password=''
    
```

https://kubernetes.io/docs/concepts/configuration/secret/#using-secrets-as-environment-variables
````

````