```
username="$(kubectl get secret rabbitmq-limited-default-user -o jsonpath='{.data.username}' | base64 --decode)"
echo "username: $username"
password="$(kubectl get secret rabbitmq-limited-default-user -o jsonpath='{.data.password}' | base64 --decode)"
echo "password: $password"
```
