Setup Ingress
https://cert-manager.io/docs/tutorials/acme/ingress/

Setup
helm install \
cert-manager jetstack/cert-manager \
--namespace cert-manager \
--create-namespace \
--version v1.3.1 \
--set installCRDs=true