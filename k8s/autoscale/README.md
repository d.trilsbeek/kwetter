### Apply kubernetes metrics server
```
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
```

### Apply autoscaler
```
kubectl apply -f ./k8s/autoscale
```

### Test with load

Tweets Api
```
kubectl run -i --tty load-generator-tweets --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://tweets-api-service/api/tweets/; done"
```

Follows Api
```
kubectl run -i --tty load-generator-follows --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://follows-api-service/api/follows/users/dtrilsbeek; done"
```

Users Api
```
kubectl run -i --tty load-generator-users --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://users-api-service/api/users/dtrilsbeek; done"
```

Identity
```
kubectl run -i --tty load-generator-identity --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://identity-service/; done"
```

